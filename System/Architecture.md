# Data logger

* **SYSARCH-1**: MCU: ESP32, SDK: IDF
* **SYSARCH-2**: Wireless technology used is LoRa with module SX1278 Ra-02 for both data logger and node devices.
* **SYSARCH-19**: In every 30 minutes, data logger queries node 1 data then queries node 2 data right after that. **Downstream [SWREQ-2](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/Software/Requirements.md#swreq-2)**
* **SYSARCH-14**: data logger gets node device info by HTTP protocol. **Downstream SWREQ-3**
* **SYSARCH-12**: Send sensor data to ThingsBoard by MQTT protocol. **Downstream SWREQ-4**
* **SYSARCH-5**: Control 2 relay by I2C bus with PCF8574
* **SYSARCH-13**: Control 2 relay by MQTT protocol. **Downstream SWREQ-4**.
* **SYSARCH-6**: Hardcoded WiFi config webpage to SPIFFS in ESP32, non support WiFi config webpage updated by FOTA. **Upstream SYSREQ-3**, **Downstream SWREQ-6**.
* **SYSARCH-7**: Total numbers of physical devices with hardware version
	* 1 data logger with hardware version 0.2.0
	* 2 node devices with hardware version 2.1
	* 1 SHT30 and 1 VMS-3001-TR--N01 for 1 node so need total 2 SHT30 and 2 VMS-3001-TR--N01

* [SYSARCH-15](#sysarch-15): Handle FOTA
* [SYSARCH-16](#sysarch-16): Antenna parameters used in data logger

## SYSARCH-15

Use MQTT for FOTA handling: Data logger subscribes to a MQTT channel to listen to a new firmware update

Defined by system architecture/software developer.

Relationship: Don't have any upstream as don't come from any system requirements. FOTA is intended for developer maintenance purpose

## SYSARCH-16

Antenna Lora RF 433 MHz SMA

| Attributes | Values |
| ------- |:------:|
| Frequency Range| 433 MHz|
| V.S.W.R  | ≦2.0:1|
|Antenna Type | Dipole Sleeve|
|Gain  |5.0 ± 0.5dBi  |
|Polarization   |Linear  |
|Horizontal Beam-with |360'|
|Impedance |50Ω|
|Weight| 50g|
|Operating Temperature|-20'C to 70'C|
|Length|14.5 cm|

Attachments: SIS02-ATT-data_logger_antenna

Upstream SYSREQ-1

# Node device

* **SYSARCH-8**: MCU ESP32, SDK: IDF
* **SYSARCH-3**: SHT30 for environment humidity and temperature measurement
* **SYSARCH-4**: VMS-3001-TR--N01 for soild humidity and temperature measurement
* [SYSARCH-18](#SYSARCH-18): Able to detect if battery voltage powering node has current voltage greater than 3V or not. If it is greater than 3V, operates node device. If it is less than 3V, stop node device.
* **SYSARCH-20**: Support power saving mode. **Upstream SYSREQ-14**

* **SYSARCH-17**: Antenna parameters used in node devices

Antenna Lora RF 433 MHz 3dBi SMA Male

| Attributes | Values |
| ------- |:------:|
| Frequency Range| 433 MHz|
| V.S.W.R  | ≦2.0:1|
|Antenna Type | Dipole Sleeve|
|Gain  |3.0 ± 0.5dBi  |
|Polarization   |Linear  |
|Horizontal Beam-with |360'|
|Impedance |50Ω|
|Weight| 7.5g|
|Operating Temperature|-20'C to 70'C|
|Longs|11 cm|

## SYSARCH-18

Defined by system architecture/software developer for Lithium battery safety purpose

Upstream SYSREQ-13

# Control panel

* **SYSARCH-9**: Implement control panel by ThingsBoard dashboard for **SYSREQ-6**, **SYSREQ-7**, **SYSREQ-8**, **SYSREQ-9**, **SYSREQ-10**, **SYSREQ-11**.
* **SYSARCH-10**: IoTDev Admin account (as tenant administrator) is able to CRUD 2 node devices for customer account. Upstream [SYSREQ-15]().
* [SYSARCH-11](#SYSARCH-11): FOTA update for data logger by a ThingsBoard dashboard, which is only accessible to IoTDev Admin account in an admin webpage. Customer user account stormnguyen1247@gmail.com can't access this webpage

## SYSARCH-11

FOTA dashboard broadcasts a MQTT message to notify physical data logger for a new software version
