# PID Temperature Controller

* [SWREQ-PIDTC-1](#swreq-pidtc-1): ESP-IDF OneWire library for DS18B20 for OneWire communication, version 0.1.0

* [SWREQ-PIDTC-2](#swreq-pidtc-2): Data logger queries slave sensors data of 2 nodes by pooling mechanism every 30 minutes. 

* [SWREQ-PIDTC-3](#swreq-pidtc-3): ESP-IDF ThingsBoard HTTP library to get node device info, version 0.1.0

* [SWREQ-4](#swreq-4): ESP-IDF MQTT library for MQTT publish and subscribe, version 0.1.0

* **SWREQ-5**: I2C functions to control relay by PCF8574

* **SWREQ-6**: WiFi manager library with features from [SYSREQ-3](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Requirements.md#sysreq-3), version 0.1.0, hardcoded WiFi config webpage to SPIFFS (upstream [SYSREQ-3](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Requirements.md#sysreq-3)).

* [SWREQ-7](#swreq-7): ESP-IDF FOTA library

* [SWREQ-17](#swreq-17): Labeling software version

## SWREQ-PIDTC-1

ESP-IDF LoRa library for SX1278 Ra-02, version 0.1.0, for LoRa communication in 2 mode:
* Point-to-point (Use this mode for unit testing in this project or for inheritance in other project)
* One master - many slave: Main model in this project where data logger is the master which queries data from 2 nodes as slaves

Upstream [SYSARCH-2](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Architecture.md)

## SWREQ-PIDTC-2

In every 30 minutes, data logger takes 2 steps below as pooling mechanism

* **Step 1**: Data logger queries sensors data from node 1 then sends that sensors data to ThingsBoard.
* **Step 2**: Data logger queries sensors data from node 2 then sends that sensors data to ThingsBoard. (Perform this step right after step 1)

Upstream [SYSARCH-19](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Architecture.md)

## SWREQ-PIDTC-3

ESP-IDF ThingsBoard HTTP library, version 0.1.0, supports:

* Get data logger neccessary credentials
* Get node devices neccessary credentials

## SWREQ-4

ESP-IDF MQTT library, version 0.1.0, supports:
* Publish MQTT message of 2 nodes sensor data to ThingsBoard
* Get subscribe MQTT message to control relay by PCF8574

## SWREQ-17

Label software version as 0.1.0 after deploying SIS02 for the first time for both case successfully deployed and failure deployed with issues.

If fail to deploy for the first time due to hardware issue (data logger board hardware version 0.2.0), fix hardware issue with new board version. 

There will be 2 cases:
* If software version 0.1.0 runs properly with that hardware version, keep software version as 0.1.0. 
* If software version 0.1.0 fails to run with that hardware version, update software version and version it later based on this software implementation

If fail to deploy for the first time due to software issue, fix by FOTA then change software version based on this software implementation.

Software version 0.1.0 after code freeze includes:

* ESP-IDF LoRa library for SX1278 Ra-02, version 0.1.0
* ESP IDF MQTT: 0.1.0
* ThingsBoard library: 0.1.0
* WiFi manager library : 0.1.0
* FOTA: 0.1.0
* Other left data logger features run as expected in its system requirements.

For any later software updating, version software based on standard software version convention.

## SWREQ-7

ESP-IDF FOTA library version 0.1.0:

* Support OTA from ThingsBoard dashboard with both HTTP and HTTPS
* Widget configuration and setup: [link](https://github.com/TranPhucVinh/ESP-IDF/tree/master/Application%20layer/FOTA#ota-with-http-server)

# Node device

* **SWREQ-8**: I2C lib for SHT30
* **SWREQ-9**: Modbus RTU for SHT30
* **SWREQ-10**: Use ADC pin to detect if battery voltage powering node has current voltage greater than 3V or not. If it is greater than 3V, operates node device. If it is less than 3V, stop node device. (Upstream **SYSARCH-18**)
* **SWREQ-11**: Support power saving mode (Upstream **SYSREQ-14**)
* **SWREQ-18**:  1 node is identified by 4 params:

* access token on ThingsBoard -> used for online/offline status and setup alarm
* working frequency
* ID for LoRa querying
* LoRa syncword

# ThingsBoard

* **SWREQ-12**: Map widget to cover **SYSREQ-7**
* **SWREQ-13**: A widget to cover **SYSREQ-8**
* **SWREQ-14**: A widget to cover **SYSREQ-9**
* **SWREQ-15**: Widgets to cover **SYSREQ-10**
* **SWREQ-16**: Widgets to cover **SYSREQ-11**
